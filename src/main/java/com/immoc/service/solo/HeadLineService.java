package com.immoc.service.solo;

import com.immoc.entity.bo.HeadLine;
import com.immoc.entity.dto.Result;

import java.util.List;

public interface HeadLineService {
    Result<Boolean> addHeadLine(HeadLine headLine);
    Result<Boolean> removeHeadLine(int headLine);
    Result<Boolean> modifyHeadLine(HeadLine headLine );
    Result<HeadLine>queryHeadLineById(int headLineId);
    Result<List<HeadLine>> queryHeadLine(HeadLine headLineCondition,int pageIndex,int pageSize);

}
