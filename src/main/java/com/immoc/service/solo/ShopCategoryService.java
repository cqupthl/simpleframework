package com.immoc.service.solo;


import com.immoc.entity.bo.ShopCategory;
import com.immoc.entity.dto.Result;

import java.util.List;

public interface ShopCategoryService {
    Result<Boolean> addShopCategory(ShopCategory headLine);
    Result<Boolean> removeShopCategory(int headLine);
    Result<Boolean> modifyShopCategory(ShopCategory headLine );
    Result<ShopCategory>queryShopCategoryById(int headLineId);
    Result<List<ShopCategory>> queryShopCategory(ShopCategory ShopCategoryCondition, int pageIndex, int pageSize);
}
