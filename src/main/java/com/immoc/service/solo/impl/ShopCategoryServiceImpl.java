package com.immoc.service.solo.impl;

import com.immoc.entity.bo.ShopCategory;
import com.immoc.entity.dto.Result;
import com.immoc.service.solo.ShopCategoryService;

import java.util.List;

/**
 * @ClassName ShopCategoryServiceImpl
 * @Description: TODO
 * @Author hl
 * @Date 2021/4/26
 * @Version V1.0
 **/
public class ShopCategoryServiceImpl implements ShopCategoryService {
    @Override
    public Result<Boolean> addShopCategory(ShopCategory headLine) {
        return null;
    }

    @Override
    public Result<Boolean> removeShopCategory(int headLine) {
        return null;
    }

    @Override
    public Result<Boolean> modifyShopCategory(ShopCategory headLine) {
        return null;
    }

    @Override
    public Result<ShopCategory> queryShopCategoryById(int headLineId) {
        return null;
    }

    @Override
    public Result<List<ShopCategory>> queryShopCategory(ShopCategory ShopCategoryCondition, int pageIndex, int pageSize) {
        return null;
    }
}
