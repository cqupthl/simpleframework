package com.immoc.service.solo.impl;

import com.immoc.entity.bo.HeadLine;
import com.immoc.entity.dto.Result;
import com.immoc.service.solo.HeadLineService;

import java.util.List;

/**
 * @ClassName HeadLineServiceImpl
 * @Description: TODO
 * @Author hl
 * @Date 2021/4/26
 * @Version V1.0
 **/
public class HeadLineServiceImpl implements HeadLineService {
    @Override
    public Result<Boolean> addHeadLine(HeadLine headLine) {
        return null;
    }

    @Override
    public Result<Boolean> removeHeadLine(int headLine) {
        return null;
    }

    @Override
    public Result<Boolean> modifyHeadLine(HeadLine headLine) {
        return null;
    }

    @Override
    public Result<HeadLine> queryHeadLineById(int headLineId) {
        return null;
    }

    @Override
    public Result<List<HeadLine>> queryHeadLine(HeadLine headLineCondition, int pageIndex, int pageSize) {
        return null;
    }
}
