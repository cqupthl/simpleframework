package com.immoc.service.combine.impl;

import com.immoc.entity.bo.HeadLine;
import com.immoc.entity.bo.ShopCategory;
import com.immoc.entity.dto.MapageInfroDTO;
import com.immoc.entity.dto.Result;
import com.immoc.service.combine.HeadLineShopCategoryService;
import com.immoc.service.solo.HeadLineService;
import com.immoc.service.solo.ShopCategoryService;

import java.util.List;

/**
 * @ClassName HeadLineShopCategoryServiceImpl
 * @Description: TODO
 * @Author hl
 * @Date 2021/4/26
 * @Version V1.0
 **/
public class HeadLineShopCategoryServiceImpl implements HeadLineShopCategoryService {
    private HeadLineService headLineService;
    private ShopCategoryService shopCategoryService;

    @Override
    public Result<MapageInfroDTO> getMainPageInfo() {
        HeadLine headLine=new HeadLine();
        headLine.setEnableStatus(1);
        Result<List<HeadLine>> heaqdLineResult=headLineService.queryHeadLine(headLine,1,4);

        ShopCategory shopCategory=new ShopCategory();
        Result<List<ShopCategory>> shopCategoryResult=shopCategoryService.queryShopCategory(shopCategory,1,100);

        return mergeHeadLineShopCategory(heaqdLineResult,shopCategoryResult);
    }

    private Result<MapageInfroDTO> mergeHeadLineShopCategory(Result<List<HeadLine>> heaqdLineResult, Result<List<ShopCategory>> shopCategoryResult) {
         Result<MapageInfroDTO> mapageInfroDTOResult=new Result<>();
         MapageInfroDTO mapageInfroDTO=new MapageInfroDTO();
         mapageInfroDTO.setHeadLineList(heaqdLineResult.getData());
         mapageInfroDTO.setShopCategoryList(shopCategoryResult.getData());
         mapageInfroDTOResult.setData(mapageInfroDTO);
         return mapageInfroDTOResult;
    }
}
