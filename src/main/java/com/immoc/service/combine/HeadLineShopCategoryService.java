package com.immoc.service.combine;

import com.immoc.entity.dto.MapageInfroDTO;
import com.immoc.entity.dto.Result;

public interface HeadLineShopCategoryService {
    Result<MapageInfroDTO> getMainPageInfo();
}
