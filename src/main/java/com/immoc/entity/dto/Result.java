package com.immoc.entity.dto;

import lombok.Data;

/**
 * @ClassName Result
 * @Description: TODO
 * @Author hl
 * @Date 2021/4/26
 * @Version V1.0
 **/
@Data
public class Result<T> {
    private int code;
    private String msg;
    private T data;
}
