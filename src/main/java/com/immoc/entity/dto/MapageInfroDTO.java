package com.immoc.entity.dto;

import com.immoc.entity.bo.HeadLine;
import com.immoc.entity.bo.ShopCategory;
import lombok.Data;

import java.util.List;

/**
 * @ClassName MapageInfroDTO
 * @Description: TODO
 * @Author hl
 * @Date 2021/4/26
 * @Version V1.0
 **/
@Data
public class MapageInfroDTO {
    private List<HeadLine> headLineList;
    private List<ShopCategory> shopCategoryList;
}
