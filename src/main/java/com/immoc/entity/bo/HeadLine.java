package com.immoc.entity.bo;

import lombok.Data;

import java.util.Date;

/**
 * @ClassName HeadLine
 * @Description: TODO
 * @Author hl
 * @Date 2021/4/25
 * @Version V1.0
 **/
@Data
public class HeadLine {
    private Long lineId;

    private String lineName;

    private String lineLink;

    private String lineImg;

    private Integer priority;

    private Integer enableStatus;

    private Date createTime;

    private Date lastEditTime;

}
