package com.immoc.entity.bo;

import lombok.Data;

import java.util.Date;

/**
 * @ClassName ShopCategory
 * @Description: TODO
 * @Author hl
 * @Date 2021/4/25
 * @Version V1.0
 **/
@Data
public class ShopCategory {
    private Long shopCategoryId;
    private String shopCategoryIdName;
    private String shopCategoryDesc;
    private String shopCategoryImg;
    private String lineImg;
    private Integer priority;
    private Integer enableStatus;
    private Date createTime;
    private Date lastEditTime;
    private ShopCategory parent;

}
